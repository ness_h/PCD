﻿<!DOCTYPE html> 
<html>
<?php
session_start();
include ('includeall.php');?>
<head>
  <title>Application de gestion du personnel </title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta http-equiv="content-type" content="text/html; charset=windows-1252" />
  <link rel="stylesheet" type="text/css" href="css/stylesupp.css" />
  <!-- modernizr enables HTML5 elements and feature detects -->
  
</head>


<body>
  
    <header>
	  <div id="main">
   <div id="banner">
   </div> 
    <div id="welcome">
      <center><b>  <h3 >Ecole Nationale des Sciences de l'Informatique <span>ENSI</span></h3></b></center>
      </div>
</div>
    </header>


	<nav>
	  <div id="menubar">
        <ul id="nav">
                   <?php menu7($type);?>


        </ul>
      </div><!--close menubar-->	
	</nav>
    
	<div id="site_content">		

	  <div class="sidebar_container"> 
<?php
	
		
		if(isset($_GET['id']) && is_numeric($_GET['id'])) {
			// Traitement de la suppression de l'événement
			$id = htmlentities($_GET['id']);
			
			$req = "DELETE FROM vacance WHERE id_event = " .$id;
			$r0=$pdo->query($req);
			
			$req = "DELETE FROM event WHERE id_event = " .$id;
			$r1=$pdo->query($req);
			
			echo '<ul><li>Evénement supprimé !</li></ul>';
		}
		
		
		// Récupération des événements
		$req = "SELECT * FROM event";
		$event = $pdo->query($req);
		$etat = $event->rowCount();
		if($etat!=NULL) $nbEvents = true;
		else $nbEvents = false;
		
	?>
    
	<h2><font color='#FFAEC9'><i>Supprimer un événement :</font></i></h2>
	
    <?php
	if($nbEvents) {
		
		while($evenement = $event->fetch()) {
			echo '
			<table class="listeEvent">
				<tr><td>'.html_entity_decode($evenement->titre).'</td></tr>
				<tr><td>'.html_entity_decode($evenement->contenu).'</td></tr>
				<tr><td><a href="supprevent.php?id='.$evenement->id_event.'">Supprimer</a></td></tr>
			</table>
			<br/><br/>
			';
		}
		
	} else {
		
		echo '<p>Il n\'y a pas d\'événements à supprimer</p>';
		
	}
	?>
    
    
  
 </div><!--close sidebar_container-->	
	
	  <div id="content">
        <div class="content_item">
		

    
		      <p> <a href="http://localhost/pcd/admin/view/menu_configuration.php">Retour à la page d'accueil</a> <p> <br/>
		  		  
		  </div><!--close content_container-->	
			 
		  
		</div><!--close content_item-->

      </div><!--close content-->   

	</div><!--close site_content-->  	
  </div><!--close main-->
  
      <footer>
	<div id="footer">Campus Universitaire de la Manouba, Manouba 2010, Tunisie</div> 
		<br>
	<?php
  echo date('d / m / Y H:i'); 
  echo"<br><br>";
logout(); ?>
 <br>   </footer>

  <!-- javascript at the bottom for fast page loading -->
  <script type="text/javascript" src="js/jquery.js"></script>
      
    

  
</body>
</html>
