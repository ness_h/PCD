
<html>
<?php

class EMPLOYE {
		var $id;
		var $nom;
		var $prenom;
		var $cin_pass;
		var $sexe;
		var $datenaiss;
		var $lieunaiss;
		var $nompere;
		var $nommere;
		var $diplome;
		var $nationalite;
		var $email;
		var $tel;
		var $ville;
		var $profession;
		var $codepost;
		var $etatcivil;
		var $type;
		var $date_recrutement;

  public function constr() {
   
	$this->nom = "";
    $this->prenom = "";
	$this->cin_pass="";
	$this->sexe="";
	$this->datenaiss="";
	$this->lieunaiss="";
	$this->nompere="";
	$this->nommere="";
	$this->diplome="";
	$this->nationalite="";
	$this->email="";
	$this->tel="";
	$this->ville="";
	$this->profession="";
	$this->codepost="";
	$this->etatcivil="";
	$this->type="";
	$this->date_recrutement=""; 
	
	$message='Ajout avec succes';
 
echo '<script type="text/javascript">window.alert("'.$message.'");</script>';
	
	
  }
  public function ajouter($bd)
  {
	  
	  $model=new Model_EMPLOYE();
	  $model->insert($bd,$this->nom, $this->prenom, $this->cin_pass,$this->sexe,$this->datenaiss,$this->lieunaiss,$this->nompere,$this->nommere,$this->diplome,$this->nationalite,$this->email,$this->tel,$this->ville,$this->profession,$this->codepost,$this->etatcivil,$this->type,$this->date_recrutement);
	  
	  
  }
   public function ajouter_perm($bd)
  {
	  
	  $model=new Model_EMPLOYE();
	  $model->insert_perm($bd,$this->nom, $this->prenom, $this->cin_pass,$this->sexe,$this->datenaiss,$this->lieunaiss,$this->nompere,$this->nommere,$this->diplome,$this->nationalite,$this->email,$this->tel,$this->ville,$this->profession,$this->codepost,$this->etatcivil,$this->type,$this->date_recrutement);
	  
	  
  }

public function set_id($id){$this->id=$id;}
public function set_nom ($nom){$this->nom =$nom;}
public function set_prenom ($prenom){$this->prenom =$prenom;}
public function set_cin_pass ($cin_pass){$this->cin_pass =$cin_pass;}
public function set_sexe ($sexe){$this->sexe =$sexe;}
public function set_datenaiss ($datenaiss){$this->datenaiss =$datenaiss;}
public function set_lieunaiss ($lieunaiss){$this->lieunaiss =$lieunaiss;}
public function set_nompere ($nompere){$this->nompere =$nompere;}
public function set_nommere ($nommere){$this->nommere =$nommere;}
public function set_diplome ($diplome){$this->diplome =$diplome;}
public function set_nationalite ($nationalite){$this->nationalite =$nationalite;}
public function set_email ($email){$this->email =$email;}
public function set_tel ($tel){$this->tel =$tel;}
public function set_ville ($ville){$this->ville =$ville;}
public function set_profession ($profession){$this->profession =$profession;}
public function set_codepost ($codepost){$this->codepost =$codepost;}
public function set_etatcivil ($etatcivil){$this->etatcivil =$etatcivil;}
public function set_type ($type){$this->type =$type;}
public function set_date_recrutement ($date_recrutement){$this->date_recrutement =$date_recrutement;}

public function get_type($idemp,$bd) {$model=new Model_EMPLOYE(); return $model->get_type($idemp);}
  
  public function modifier_emp($bd) { 
$model=new Model_EMPLOYE();
if($this->nom!=""){$model->modify_nom($bd,$this->nom,$this->id);}
if($this->prenom!=""){$model->modify_prenom($bd,$this->prenom,$this->id);}
if($this->sexe!=""){$model->modify_sexe($bd,$this->sexe,$this->id);}
if($this->cin_pass!=""){$model->modify_cin_pass($bd,$this->cin_pass,$this->id);}
if($this->datenaiss!=""){$model->modify_datenaiss($bd,$this->datenaiss,$this->id);}
if($this->lieunaiss!=""){$model->modify_lieunaiss($bd,$this->lieunaiss,$this->id);}
if($this->nompere!=""){$model->modify_nompere($bd,$this->nompere,$this->id);}
if($this->nommere!=""){$model->modify_nommere($bd,$this->nommere,$this->id);}
if($this->diplome!=""){$model->modify_diplome($bd,$this->diplome,$this->id);}
if($this->nationalite!=""){$model->modify_nationalite($bd,$this->nationalite,$this->id);}
if($this->email!=""){$model->modify_email($bd,$this->email,$this->id);}
if($this->tel!=""){$model->modify_tel($bd,$this->tel,$this->id);}
if($this->ville!=""){$model->modify_ville($bd,$this->ville,$this->id);}
if($this->profession!=""){$model->modify_profession($bd,$this->profession,$this->id);}
if($this->date_recrutement!=""){$model->modify_date_recrutement($bd,$this->date_recrutement,$this->id);}
if($this->codepost!=""){$model->modify_codepost($bd,$this->codepost,$this->id);}
if($this->type!=""){$model->modify_type($bd,$this->type,$this->id);}
if($this->etatcivil!=""){$model->modify_etatcivil($bd,$this->etatcivil,$this->id);}
	echo "cet employe est mis à jour avec succes";
 }
 
  
   public function supp($bd)
  { $model=new Model_EMPLOYE();
  $model->supprimer($bd,$this->id);
  $message='cet employe est supprimé avec succès';
 echo '<script type="text/javascript">window.alert("'.$message.'");</script>';}
 
 public function supp_perm($bd)
  {$model=new Model_EMPLOYE();
  $model->supprimer_perm($bd,$this->id);
  }

}





?>
</html>