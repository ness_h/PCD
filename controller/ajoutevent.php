﻿<!DOCTYPE html> 
<html>
<?php 
session_start();
include ('includeall.php');
?>
<head>
  <title>Application de gestion du personnel </title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta http-equiv="content-type" content="text/html; charset=windows-1252" />
  <link rel="stylesheet" type="text/css" href="css/stylesupp.css" />
  <!-- modernizr enables HTML5 elements and feature detects -->
  <script type="text/javascript" src="js/modernizr-1.5.min.js"></script>
</head>

<body>
  
    <header>
	  <div id="main">
   <div id="banner">
   </div> 
    <div id="welcome">
      <center><b> <h1>Calendrier</h1></b></center>
      </div>
</div>
    </header>


	  <div id="menubar">
        <ul id="nav">
                 <?php menu7($type);?>

        </ul>
      </div><!--close menubar-->	
    
    
	<div id="site_content">		

	  <div class="sidebar_container">       
	

	<!-- Formulaire d'envoi -->
	<h2><font color='#FFAEC9'><i>Veuillez ajouter un événement :</i> </font></h2>
    <?php // Variables vides pour les valeurs par défaut des champs
		$titre=""; $description=""; $dateDebut = date("d/m/Y", time()); $dateFin = date("d/m/Y", time());?>
    <form method="post" action="pendingevent.php">
    	<table id="tabAjoutEvent">
        	<tr>
            	<td><label>Du : <input type="text" name="debut" value="<?php echo $dateDebut ?>" /></label></td>
                <td><label>Au : <input type="text" name="fin" value="<?php echo $dateFin; ?>" /></label></td>
            </tr>
       		<tr>
       			<td colspan="2"><br/>
                	<label for="titre">Titre de l'événement :</label><br/>
       				<input type="text" name="titre" id="titre" size="30" value="<?php echo $titre ?>" /><br/><br/>
                </td>
       		</tr>
            <tr>
            	<td colspan="2">
       				<label for="description">Description de l'événement :</label><br/>
       				<textarea rows="10" cols="50" id="description" name="description"><?php echo $description ?></textarea>
                </td>
            </tr>
            <tr></table>
            	<div class="valid"><input type="submit" value="Envoyer"></div>
      
    </form>
       </div><!--close sidebar_container-->	<br/>
	
	  <div id="content">
        <div class="content_item">
       <p> <a href="http://localhost/pcd/view/calendADMIN.php">Retour à la page précédante</a><br/>                                       
 <a href="http://localhost/pcd/view/menu_configuration.php">Retour à la page d'accueil</a> </p> <br/>
		  
          <div class="content_container">
		        
		  		  
		  </div><!--close content_container-->			  
		</div><!--close content_item-->
      </div><!--close content-->   
	</div><!--close site_content-->  	
  </div><!--close main-->
  
     <footer>
	<div id="footer">Campus Universitaire de la Manouba, Manouba 2010, Tunisie</div> 
		<br>
	<?php
  echo date('d / m / Y H:i'); 
  echo"<br><br>";
logout(); ?>
 <br>   </footer>

  <!-- javascript at the bottom for fast page loading -->
  <script type="text/javascript" src="js/jquery.js"></script>
      
    

  
</body>
</html>
