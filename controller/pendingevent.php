﻿<?php
		
		include ('ajoutevent.php');
		
		
			// Traitement de l'envoi de l'événement
			$titre = htmlentities(addslashes($_POST['titre']));
			$description = nl2br(htmlentities(addslashes($_POST['description'])));
			$dateDebut = htmlentities($_POST['debut']);
			$dateFin = htmlentities($_POST['fin']);
			
			$typeDate = "#^[0-3]?[0-9]/[01]?[0-9]/[0-9]{4}$#";
			
			if (preg_match($typeDate, $dateDebut) && preg_match($typeDate, $dateFin)) {
				$tabDateDeb = explode("/", $dateDebut);
				$timestampDebut = mktime(0, 0, 0, $tabDateDeb[1], $tabDateDeb[0], $tabDateDeb[2]);
				
				$tabDateFin = explode("/", $dateFin);
				$timestampFin = mktime(0, 0, 0, $tabDateFin[1], $tabDateFin[0], $tabDateFin[2]);
				
				$timestampDiff = $timestampFin - $timestampDebut;
				$nbreJours = intval($timestampDiff / 86400)+1;
				
				if($nbreJours <= 0) $nbreJours = 1;
				
				
				if(!empty($titre) && !empty($description)) {
					// Traitement de l'enregistrement de l'événement
					$identifiantCommun = time();
					$timeDuJour = $timestampDebut;
					
					
					
					for($i=0 ; $i<$nbreJours ; $i++) {
						$req = "INSERT INTO vacance VALUES ('', ".date('d', $timeDuJour).", ".date('m', $timeDuJour).", ".date('Y', $timeDuJour).", $identifiantCommun)";
						$r0=$pdo->query($req);
						
						$timeDuJour += 86400; // On augmente le timestamp d'un jour
					}
					
					$req = "INSERT INTO event VALUES ($identifiantCommun, '$titre', '$description')";
					$r1=$pdo->query($req);
					
					
					echo '<ul><li>Evénement enregistré !</li></ul>';
				} else {
					echo '<ul><li>Titre ou description de l\'événement non renseigné.</li></ul>';
				}
			}
			else
			{
				echo '<ul><li>Date de début ou de fin d\'événement non conforme (ex. 12/02/2008).</li></ul>';
			}
		
	?>