<?php // MyPDO
 //la classe MyPdo h�rite de la classe PDO (extends)
class MyPdo extends PDO {
/* on privil�gie les var de classe (statiques) aux constantes : ainsi modifiables par programme */
	static public $DB_NAME = "pcd";
 
	static public $HOST = "localhost";
 
	static public $USER = "root";
 
	static public $PASS = "";
 
 
 
// le constructeur de MyPdo appelle le constructeur de PDO en lui passant ses param�tres	
	function __construct() {
	$pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
	$pdo_options[PDO::ATTR_EMULATE_PREPARES] = false;//important sur les configs r�centes
	$pdo_options[PDO::ATTR_DEFAULT_FETCH_MODE] = PDO::FETCH_OBJ;//pour le mode objet
	$pdo_options[PDO::MYSQL_ATTR_INIT_COMMAND] = "SET NAMES utf8";//pour l'utf-8
 
	parent::__construct('mysql:host=' . MyPdo::$HOST . ';dbname=' . MyPdo::$DB_NAME, MyPdo::$USER, MyPdo::$PASS, $pdo_options);
	}
 
}
// fin MyPDO
 ?>
