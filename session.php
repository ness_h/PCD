
<html>
<?php

class SESSION {
	var $usern;
	var $motdp;
	var $id; 
	
	public function set_user($usern,$motdp)
	{
		$this->usern=$usern;
	     $this->motdp=$motdp;
	}
  public function get_session_id() {
    $aut=new Model_AUT();
	$this->id=$aut->get_session_id($this->usern,$this->motdp);
	return $this->id;
  }
  public function get_session_nom() {
  $aut=new Model_AUT();
	return $aut->get_session_nom($this->id);
  }
  public function get_session_prenom() {
     $aut=new Model_AUT();
	return $aut->get_session_prenom($this->id);
  }
  public function get_session_sexe() {
     $aut=new Model_AUT();
	return $aut->get_session_sexe($this->id);
  }
  public function get_session_cin_pass() {
     $aut=new Model_AUT();
	return $aut->get_session_cin_pass($this->id);

  }
  public function get_session_datenaiss() {
    
	$aut=new Model_AUT();
	return $aut->get_session_datenaiss($this->id);
  }
  public function get_session_lieunaiss() {
    
	$aut=new Model_AUT();
	return $aut->get_session_lieunaiss($this->id);
  }
  public function get_session_nompere() {
    
	$aut=new Model_AUT();
	return $aut->get_session_nompere($this->id);
  }
  public function get_session_nommere() {
    
	$aut=new Model_AUT();
	return $aut->get_session_nommere($this->id);
  }
  public function get_session_diplome() {
    
	$aut=new Model_AUT();
	return $aut->get_session_diplome($this->id);
  }
  public function get_session_nationalite() {
    
	$aut=new Model_AUT();
	return $aut->get_session_nationalite($this->id);
  }
  public function get_session_email() {
    
	$aut=new Model_AUT();
	return $aut->get_session_email($this->id);
  }
  public function get_session_tel() {
    
	$aut=new Model_AUT();
	return $aut->get_session_tel($this->id);
  }
  public function get_session_ville() {
    
	$aut=new Model_AUT();
	return $aut->get_session_ville($this->id);
  }
  public function get_session_profession() {
    
	$aut=new Model_AUT();
	return $aut->get_session_profession($this->id);
  }
  public function get_session_codepost() {
    
	$aut=new Model_AUT();
	return $aut->get_session_codepost($this->id);
  }
  public function get_session_etatcivil() {
    
	$aut=new Model_AUT();
	return $aut->get_session_etatcivil($this->id);
  }
  public function get_session_type() {
    
	$aut=new Model_AUT();
	return $aut->get_session_type($this->id);
  }
  public function get_session_date_recrutement() {
    
	$aut=new Model_AUT();
	return $aut->get_session_date_recrutement($this->id);
  }
  
  
  
}



?>
</html>