<!DOCTYPE html> 
<html>
<?php 
session_start();
include ('includeall.php');
include('jpgraph/src/jpgraph.php');
include('jpgraph/src/jpgraph_line.php');
?>
<head>
  <title>Application de gestion du personnel </title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta http-equiv="content-type" content="text/html; charset=windows-1252" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <!-- modernizr enables HTML5 elements and feature detects -->
  
</head>


<body>
  
    <header>
	  <div id="main">
   <div id="banner">
   </div> 
    <div id="welcome">
      <center><b>  <h3 >Ecole Nationale des Sciences de l'Informatique <span>ENSI</span></h3></b></center>
      </div>
</div>
    </header>


	<nav>
	  <div id="menubar">
        <ul id="nav">
          <?php menu4($type); ?>

        </ul>
      </div><!--close menubar-->	
	</nav>
    
	<div id="site_content">		

	  <div class="sidebar_container"> 

  
 
<h1>Graphe :</h1>
 
 </div><!--close sidebar_container-->	
	
	  <div id="content">
        <div class="content_item">
		

		  
		  
          <div class="content_container">
		 <form method="post" action="pendingconfig.php">
    		
  			<div class="inset">
				
	  			
<?php
$x_axis = array();
$i = 0;
$query=('SELECT COUNT(*) AS nombre ,YEAR(date) AS year FROM pointage WHERE etat="absent" GROUP BY YEAR(date) ORDER BY YEAR(date)');
$req=$pdo->query($query);

while($row=$req->fetch())
{
	$x_axis[$i] =  $row->nombre;
    $i++;
}
$graph = new Graph(400,200);
$graph->img->SetMargin(40,40,40,40);  
$graph->img->SetAntiAliasing();
$graph->SetScale("intint",0,0,0,5);
$graph->SetShadow();
$graph->title->Set("Evolution du nombre des absences en annees");
$graph->title->SetFont(FF_FONT1,FS_BOLD);
 
 
// Use 20% "grace" to get slightly larger scale then min/max of
// data
$graph->yscale->SetGrace(0);
$p1 = new LinePlot($x_axis);
$p1->mark->SetType(MARK_FILLEDCIRCLE);
$p1->mark->SetFillColor("blue");
$p1->mark->SetWidth(4);
$p1->SetColor("blue");
$p1->SetCenter();
$graph->Add($p1);
$graph->Stroke('images.png');
echo ('<img src="images.png">');



?>
					
 			 </div>

		</form><br><br>

<a href="presenceRH.php">Retour � la page d'accueil</a>
    
		        
		  		  
		  </div><!--close content_container-->			  
		</div><!--close content_item-->
      </div><!--close content-->   
	</div><!--close site_content-->  	
  </div><!--close main-->
  
    <footer>
	<div id="footer">Campus Universitaire de la Manouba, Manouba 2010, Tunisie</div> 
		<br>
	<?php
  echo date('d / m / Y H:i'); 
  echo"<br><br>";
logout(); ?>
 <br>   </footer>

  <!-- javascript at the bottom for fast page loading -->
  <script type="text/javascript" src="js/jquery.js"></script>
      
    

  
</body>
</html>
